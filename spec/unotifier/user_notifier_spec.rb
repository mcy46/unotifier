require "i18n"

class NotificationProvider
  def notify(notification); end

  def sensitive?
    false
  end
end

class ExternalNotificationProvider < NotificationProvider; end
class SiteNotificationProvider < NotificationProvider; end

RSpec.describe UNotifier::UserNotifier do
  let(:action_cable_server) { double("ActionCable Server") }
  let(:site_provider) { SiteNotificationProvider.new }

  let(:notification_mailer) { double("Notification Mailer") }
  let(:notification_email) { double("Notification Email") }
  let(:external_provider) { ExternalNotificationProvider.new }

  let(:target) { double("Target") }
  let(:resource_class) { double("Resource Class") }
  let(:notification_entity) { double("Notification Entity") }
  let(:notifications_path) { File.join(__dir__, "..", "fixtures", "notifications.yml") }

  let(:notifications_fixture) { YAML.load_file(notifications_path) }

  let(:key) { "first_category.n_regular" }
  let(:locale_key) { UNotifier.locale_key_for(key) }
  let(:params) { Hash.new }
  let(:user_settings) { "external" }
  let(:hide_sensitive) { false }
  let(:notification_settings) {
    {
      "user" => { key => user_settings },
      "options" => { "hide_sensitive" => hide_sensitive },
    }
  }

  before do
    UNotifier.reload!
    UNotifier.configure do |config|
      config.site_providers = [site_provider]
      config.external_providers = [external_provider]
      config.resource_class = resource_class
      config.notifications_path = notifications_path
      config.localization_provider = I18n
    end

    allow(I18n).to receive(:t).and_return("title or body")
    allow(target).to receive(:online?).and_return(true)
    allow(target).to receive(:respond_to?).and_return(false)
    allow(target).to receive(:locale).and_return(:en)
    allow(target).to receive(:notification_settings).and_return(notification_settings)
    allow(notification_entity).to receive(:key).and_return(key)
    allow(notification_entity).to receive(:urgency).and_return(notifications_fixture.dig(*key.split("."), "user", "urgency"))
    allow(notification_entity).to receive(:target).and_return(target)
    allow(notification_entity).to receive(:title=).with("title or body")
    allow(notification_entity).to receive(:body=).with("title or body")
    allow(notification_entity).to receive(:save!)
    allow(resource_class).to receive(:new).and_return(notification_entity)

    allow(I18n).to(
      receive(:exists?)
      .with(/#{locale_key}\.SiteNotificationProvider\.(body|title)/)
      .and_return(false)
    )
    allow(I18n).to(
      receive(:exists?)
      .with(/#{locale_key}\.ExternalNotificationProvider\.(body|title)/)
      .and_return(false)
    )
    allow(I18n).to(
      receive(:exists?)
      .with(/#{locale_key}\.(body|title)/)
      .and_return(true)
    )
  end

  subject { -> { UNotifier::UserNotifier.call(notification_entity, params) } }

  # Spec contents:
  # - Notifications content translation
  # - Specifying channel
  # - Urgency conditions


  # ---------------------------
  # Notifications content l10n
  # ---------------------------

  # There'e 3 flags that deterimine the resulting text of the notification:

  # custom_locale          - when there're custom locale key for specific provider
  # sensitive_notification - when provider is sensitive, notification is sensitive
  #                          and user disable sensitive info in settings
  # no_locale              - when corresponding locale key doesn't exist
  #                          (can't be combined with custom_locale)

  # Also there're 6 possible resulting translations for all flag combinations:

  # provided        - standard locale key (path.to.key.title)
  # private         - locale key with "_private" postfix (path.to.key.title_private)
  # custom          - custom key for specific provider (path.to.key.ProviderName.title)
  # custom private  - custom private key for specific provider (path.to.key.ProviderName.title_private)
  # default         - default predefined key (notifications.default.title)
  # default private - default prefefined private key (notifications.default.title_private)

  # custom_locale | sensitive_notification | no_locale |     result
  # ---------------------------------------------------------------------
  #       0       |          0             |     0     | provided
  #       0       |          0             |     1     | default
  #       0       |          1             |     0     | private
  #       0       |          1             |     1     | default private
  #       1       |          0             |     0     | custom
  #       1       |          1             |     0     | custom private

  # Each context has a description about what cases from the table above it covers

  # custom_locale: 0 | sensitive_notification: 0 | no_locale: 0
  context "when locale is provided for the notification" do
    before do
      allow(I18n).to(
        receive(:exists?)
        .with(%r{#{locale_key}.(body|title)})
        .and_return(true)
      )
    end

    it "takes provided locale" do
      expect(I18n).to(
        receive(:t)
        .with(%r{#{locale_key}.(body|title)}, hash_including(params))
        .twice
        .and_return("title or body")
      )

      expect(notification_entity).to receive(:title=).with("title or body")
      expect(notification_entity).to receive(:body=).with("title or body")

      subject.call
    end
  end

  # custom_locale: 0 | sensitive_notificaton: 0 | no_locale: 1
  context "when locale is not provided for the notification" do
    before do
      allow(I18n).to(
        receive(:exists?)
        .with(%r{#{locale_key}.(body|title)})
        .and_return(false)
      )
    end

    it "takes default locale" do
      expect(I18n).to(
        receive(:t)
        .with(%r{notifications.default.(body|title)}, hash_including(params))
        .twice
        .and_return("default title or body")
      )

      expect(notification_entity).to receive(:title=).with("default title or body")
      expect(notification_entity).to receive(:body=).with("default title or body")

      subject.call
    end
  end

  # custom_locale: 0 | sensitive_notification: 1 | no_locale: 0
  # custom_locale: 0 | sensitive_notification: 1 | no_locale: 1
  context "when user disabled sensitive information in settings" do
    let(:hide_sensitive) { true }

    context "when provider is marked as sensitive and notification is not marked as non-sensitive" do
      before do
        allow(site_provider).to receive(:sensitive?).and_return(true)
      end

      context "when private locale for the notification exists" do
        before do
          allow(I18n).to(
            receive(:exists?)
            .with(%r{#{locale_key}.title_private})
            .and_return(true)
          )
        end

        it "takes locales with '_private' postfix" do
          expect(I18n).to(
            receive(:t)
            .with(/#{locale_key}\.title_private/, hash_including(params))
            .and_return("private title")
          )

          expect(notification_entity).to receive(:title=).with("private title")
          expect(notification_entity).to receive(:body=).with("")

          subject.call
        end
      end

      context "when private locale for the notification doesn't exist" do
        before do
          allow(I18n).to(
            receive(:exists?)
            .with(%r{#{locale_key}.title_private})
            .and_return(false)
          )
        end

        it "takes default private locale" do
          expect(I18n).to(
            receive(:t)
            .with(%r{notifications.default.title_private}, hash_including(params))
            .and_return("default private title")
          )

          expect(notification_entity).to receive(:title=).with("default private title")
          expect(notification_entity).to receive(:body=).with("")

          subject.call
        end
      end
    end

    context "when provider is not marked as sensitive" do
      before do
        allow(site_provider).to receive(:sensitive?).and_return(false)
      end

      it "takes locales without postfix" do
        expect(I18n).not_to(
          receive(:t)
          .with(/#{locale_key}.(body|title)_private/, hash_including(params))
        )

        expect(notification_entity).to receive(:title=).with("title or body")
        expect(notification_entity).to receive(:body=).with("title or body")

        subject.call
      end
    end

    context "when notification is marked as non-sensitive" do
      let(:key) { "first_category.n_non_sensitive" }

      it "takes locales without postfix" do
        expect(I18n).not_to(
          receive(:t)
          .with(/#{locale_key}.(body|title)_private/, hash_including(params))
        )

        expect(notification_entity).to receive(:title=).with("title or body")
        expect(notification_entity).to receive(:body=).with("title or body")

        subject.call
      end
    end
  end

  # custom_locale: 1 | sensitive_notification: 0 | no_locale: 0
  context "when custom locale provided for a provider" do
    let(:key) { "first_category.n_immediate" }

    before do
      allow(I18n).to(
        receive(:exists?)
        .with(/#{locale_key}\.ExternalNotificationProvider\.(body|title)/)
        .and_return(true)
      )
    end

    it "takes custom locale for notification's title and body for notifying with that provider" do
      expect(I18n).to(
        receive(:t)
        .with(/#{locale_key}\.ExternalNotificationProvider\.(body|title)/, hash_including(params))
        .twice
        .and_return("custom title or body")
      )

      expect(notification_entity).to receive(:title=).with("custom title or body")
      expect(notification_entity).to receive(:body=).with("custom title or body")

      subject.call
    end

    it "doesn't take custom locale for other providers" do
      expect(I18n).not_to(
        receive(:t)
        .with(/#{locale_key}\.SiteNotificationProvider\.(body|title)/, kind_of(Hash))
      )

      expect(notification_entity).to receive(:title=).with("title or body")
      expect(notification_entity).to receive(:body=).with("title or body")

      subject.call
    end
  end

  # custom_locale: 1 | sensitive_notification: 1 | no_locale: 0
  context "when custom locale provided and user disabled sensitive information and the provider is sensitive" do
    let(:hide_sensitive) { true }

    before do
      allow(site_provider).to receive(:sensitive?).and_return(true)

      allow(I18n).to(
        receive(:exists?)
        .with(/#{locale_key}\.SiteNotificationProvider\.(body|title)/)
        .and_return(true)
      )
    end

    it "takes custom locale with '_private' postfix" do
      expect(I18n).to(
        receive(:t)
        .with(/#{locale_key}\.SiteNotificationProvider\.title_private/, hash_including(params))
        .and_return("custom private title")
      )

      expect(notification_entity).to receive(:title=).with("custom private title")
      expect(notification_entity).to receive(:body=).with("")

      subject.call
    end
  end

  # ---------------------------
  # Specifying channel
  # ---------------------------

  context "when :onsite_only parameter passed" do
    let(:key) { "first_category.n_immediate" }
    let(:params) { { onsite_only: true } }

    it { is_expected.to notify_with([site_provider]) }
    it { is_expected.not_to notify_with([external_provider]) }
  end

  context "when :external_only parameter passed" do
    let(:key) { "first_category.n_immediate" }
    let(:params) { { external_only: true } }

    it { is_expected.to notify_with([external_provider]) }
    it { is_expected.not_to notify_with([site_provider]) }
  end

  # ---------------------------
  # Urgency conditions
  # ---------------------------

  context "when target is online" do
    context "when notification urgency is 'immediate'" do
      let(:key) { "first_category.n_immediate" }

      %w(off onsite external unknown).each do |level|
        context "when user has '#{level}' setting value for the key" do
          let(:user_settings) { level }

          it { is_expected.to notify_with([site_provider, external_provider]) }
        end
      end
    end

    %w(regular onsite).each do |urgency|
      context "when notification urgency is '#{urgency}'" do
        let(:key) { "first_category.n_#{urgency}" }

        %w(off onsite external unknown).each do |level|
          context "when user has '#{level}' setting value for the key" do
            let(:user_settings) { level }

            it { is_expected.to notify_with([site_provider]) }

            it { is_expected.not_to notify_with([external_provider]) }
          end
        end
      end
    end

    context "when notification urgency is 'optional'" do
      let(:key) { "first_category.n_optional" }

      %w(onsite external unknown).each do |level|
        context "when user has '#{level}' setting value for the key" do
          let(:user_settings) { level }

          it { is_expected.to notify_with([site_provider]) }

          it { is_expected.not_to notify_with([external_provider]) }
        end
      end

      context "when user has 'off' setting value for the key" do
        let(:user_settings) { "off" }

        it { is_expected.not_to notify_with([site_provider, external_provider]) }
      end
    end
  end

  context "when target is offline" do
    before do
      allow(target).to receive(:online?).and_return(false)
    end

    context "when notification urgency is 'immediate'" do
      let(:key) { "first_category.n_immediate" }

      %w(off onsite external unknown).each do |level|
        context "when user has '#{level}' setting value for the key" do
          let(:user_settings) { level }

          it { is_expected.to notify_with([external_provider]) }

          it { is_expected.not_to notify_with([site_provider]) }
        end
      end
    end

    %w(regular optional).each do |urgency|
      context "when notificaion urgency is '#{urgency}'" do
        let(:key) { "first_category.n_#{urgency}" }

        context "when user has 'external' setting value for the key" do
          let(:user_settings) { "external" }

          it { is_expected.to notify_with([external_provider]) }

          it { is_expected.not_to notify_with([site_provider]) }
        end

        %w(off onsite unknown).each do |level|
          context "when user has '#{level}' setting value for the key" do
            let(:user_settings) { level }

            it { is_expected.not_to notify_with([site_provider, external_provider]) }
          end
        end
      end
    end

    context "when notifiction urgency is 'onsite'" do
      let(:key) { "first_category.n_onsite" }

      %w(off onsite external unknown).each do |level|
        context "when user has '#{level}' setting value for the key" do
          let(:user_settings) { level }

          it { is_expected.not_to notify_with([site_provider, external_provider])}
        end
      end
    end
  end
end
