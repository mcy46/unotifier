module UNotifier
  module Provider
    module System
      class ActionCable < ProviderBase
        attr_reader :server
        attr_accessor :channel_name

        def initialize(server, channel_name: nil, notification_conditions: [])
          super(notification_conditions: notification_conditions)
          @server = server
          @channel_name = channel_name
        end

        def notify(notification, data)
          return unless can_notify?(notification)

          server.broadcast channel_name.call(notification), data
        end

        def sensitive?
          false
        end
      end
    end
  end
end
